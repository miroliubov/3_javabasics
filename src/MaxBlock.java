package src;

/*
String-3
maxBlock
https://codingbat.com/prob/p179479

Given a string, return the length of the largest "block" in the string.
A block is a run of adjacent chars that are the same.
*/

public class MaxBlock {

    public int maxBlock(String str) {

        int count = 1;
        int maxRow = 1;

        if(str.isEmpty()) return 0;

        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i-1) == str.charAt(i)) {
                count++;
            }
            else {
                if (maxRow < count) {
                    maxRow = count;
                }
                count = 1;
            }
        }

        if (maxRow < count) {
            maxRow = count;
        }

        return maxRow;

    }

    public void execute() {
        System.out.println(maxBlock("hoopla"));
        System.out.println(maxBlock("abbCCCddBBBxx"));
        System.out.println(maxBlock(""));
    }
}
