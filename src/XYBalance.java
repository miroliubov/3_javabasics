package src;

/*
String-2
xyBalance
https://codingbat.com/prob/p134250

We'll say that a String is xy-balanced if for all the 'x' chars in the string, there exists a 'y' char
somewhere later in the string. So "xxy" is balanced, but "xyx" is not. One 'y' can balance multiple 'x's.
Return true if the given string is xy-balanced.
*/

public class XYBalance {

    public boolean xyBalance(String str) {

        boolean m = true;

        for (int i = 0; i < str.length(); ) {
            if (str.charAt(i) == 'x') {
                for (int b = i; b < str.length(); b++) {
                    if (str.charAt(b) == 'y') {
                        m = true;
                        break;
                    }
                    else {
                        m = false;
                    }
                }
            }
            i++;
        }

        return m;

    }

    public void execute() {
        System.out.println(xyBalance("aaxbby"));
        System.out.println(xyBalance("aaxbb"));
        System.out.println(xyBalance("yaaxbb"));
    }
}
