package src;

/*
Array-3
canBalance
https://codingbat.com/prob/p158767

Given a non-empty array, return true if there is a place to split the array so that
the sum of the numbers on one side is equal to the sum of the numbers on the other side.
*/

public class CanBalance {

    public boolean canBalance(int[] nums) {
        int rightSum = 0;
        int leftSum = 0;
        for (int i = 0; i < nums.length; i++) {
            rightSum += nums[i];
        }
        for (int i = 0; i < nums.length; i++) {
            leftSum += nums[i];
            rightSum -= nums[i];
            if (leftSum == rightSum) return true;
        }
        return false;
    }

    public void execute() {
        System.out.println(canBalance(new int[]{1, 1, 1, 2, 1}));
        System.out.println(canBalance(new int[]{2, 1, 1, 2, 1}));
        System.out.println(canBalance(new int[]{10, 10}));
        System.out.println(canBalance(new int[]{2, 3, 4, 1, 2}));
    }

}
