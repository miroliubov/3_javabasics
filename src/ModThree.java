package src;

/*
Array-2
modThree
https://codingbat.com/prob/p159979

Given an array of ints, return true if the array contains either 3 even or 3 odd values all next to each other.
*/

public class ModThree {

    public boolean modThree(int[] nums) {

        for (int i = 2; i < nums.length; i++) {
            if ( (nums[i] % 2 == 0 && nums[i-1] % 2 == 0 && nums[i-2] % 2 == 0)
                    || (nums[i] % 2 != 0 && nums[i-1] % 2 != 0 && nums[i-2] % 2 != 0) ) {
                return true;
            }
        }

        return false;

    }

    public void execute() {
        System.out.println(modThree(new int[]{2, 1, 3, 5}));
        System.out.println(modThree(new int[]{2, 1, 2, 5}));
        System.out.println(modThree(new int[]{2, 4, 2, 5}));
    }
}