package src;

/*
Map-2
wordCount
https://codingbat.com/prob/p117630

The classic word-count algorithm: given an array of strings, return a Map<String, Integer>
with a key for each different string, with the value the number of times that string appears in the array.
*/

import java.util.HashMap;
import java.util.Map;

public class WordCount {

    public Map<String, Integer> wordCount(String[] strings) {
        Map<String, Integer> wordCount = new HashMap<String, Integer>();
        for (int i = 0; i < strings.length; i++) {
            if (!wordCount.containsKey(strings[i])) {
                wordCount.put(strings[i], 1);
            }
            else {
                int count = wordCount.get(strings[i]);
                wordCount.put(strings[i], count + 1);
            }

        }
        return wordCount;
    }


    public void execute() {
        System.out.println(wordCount(new String[]{"a", "b", "a", "c", "b"}));
        System.out.println(wordCount(new String[]{"c", "b", "a"}));
        System.out.println(wordCount(new String[]{"c", "c", "c", "c"}));
        System.out.println(wordCount(new String[]{"d", "a", "e", "d", "a", "d", "b", "b", "z", "a", "a", "b", "z", "x", "b", "f", "x", "two", "b", "one", "two"}));
    }

}