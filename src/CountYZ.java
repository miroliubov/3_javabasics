package src;

/*
String-3
countYZ
https://codingbat.com/prob/p199171

Given a string, count the number of words ending in 'y' or 'z' -- so the 'y' in "heavy" and the 'z' in "fez"
count, but not the 'y' in "yellow" (not case sensitive). We'll say that a y or z is at the end of a word if
there is not an alphabetic letter immediately following it. (Note: Character.isLetter(char) tests if a
char is an alphabetic letter.)
*/

public class CountYZ {

    public int countYZ(String str) {
        str = str.toLowerCase();
        String[] arr = str.split("[^a-z]");
        int count = 0;
        for(int i = 0; i < arr.length; i++) {
            if (arr[i].endsWith("y") || arr[i].endsWith("z")) {
                count++;
            }
        }
        return count;
    }

    public void execute() {
        System.out.println(countYZ("fez day"));
        System.out.println(countYZ("day fez"));
        System.out.println(countYZ("DAY abc XYZ"));
        System.out.println(countYZ("!!day--yaz!!"));
    }

}