package src;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Please enter the task number (1-9): ");

        Scanner input = new Scanner(System.in);
        int value = input.nextInt();

        switch (value) {

            case(1): //Execute WordCount.java Map-2
                WordCount wordCount = new WordCount();
                wordCount.execute();
                break;

            case(2): // Execute LuckySum.java Logic-2
                LuckySum luckySum = new LuckySum();
                luckySum.execute();
                break;


            case(3): //Execute XYBalance.java String-2
                XYBalance xyBalance = new XYBalance();
                xyBalance.execute();
                break;

            case(4): //Execute MaxBlock.java String-3
                MaxBlock maxB = new MaxBlock();
                maxB.execute();
                break;

            case(5): //Execute CountYZ.java String-3
                CountYZ countYZ = new CountYZ();
                countYZ.execute();
                break;

            case(6): //Execute NotReplace.java String-3
                NotReplace notReplace = new NotReplace();
                notReplace.execute();
                break;

            case(7): //Execute SquareUp.java Array-3
                SquareUp squareUp = new SquareUp();
                squareUp.execute();
                break;

            case(8): //Execute CanBalance.java Array-3
                CanBalance canBalance = new CanBalance();
                canBalance.execute();
                break;

            case(9): //Execute ModThree.java Array-2
                ModThree modThree = new ModThree();
                modThree.execute();
                break;
        }
    }
}
